export function defaults(props,defaultProps){
    if (props === null || typeof props !== "object") {
        throw new Error("Argument must be of type object");
      }

      for(let key in props){
        if(typeof props[key] === 'undefined'){
            if(defaultProps.hasOwnProperty(key)){
                props[key] = defaultProps[key]
            }
        }
      }
      return props
}