export function keys(obj){

    if(obj === null || typeof obj !== 'object'){
        throw new Error('Argument must be of type object')
    }
    const result = []

    for(let key in obj){
        if(obj.hasOwnProperty(key)){
            result.push(key)
        }
    }
    return result;
}