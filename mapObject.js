export function mapObject(obj,cb){
    if(obj === null || typeof obj !== 'object'){
        throw new Error('Argument must be of type object')
    }
    let result = []
    for(let key in obj){
        result[key] = cb(obj[key],key,obj)
    }
    return result;
}