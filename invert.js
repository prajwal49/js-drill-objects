export function invert(obj) {
  if (obj === null || typeof obj !== "object") {
    throw new Error("Argument must be of type object");
  }
  let result = {};

  for (let key in obj) {
    result[obj[key]] = key;
  }

  return result;
}
