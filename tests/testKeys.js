import { keys } from "../keys.js";

const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };

try {
  const result = keys(testObject);
  console.log(JSON.stringify(result));
} catch (error) {
  console.log(error.message);
}
