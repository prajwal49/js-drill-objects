import { pairs } from "../pairs.js"

const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };

try {
    const result = pairs(testObject);
    console.log(result);
} catch (error) {
    console.log(error.message);
}