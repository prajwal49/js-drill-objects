import { mapObject }  from "../mapObject.js"
const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };

try {
    let result = mapObject(testObject,(value,key,obj) => {
        return `${value}`;
    })
    console.log(result);
} catch (error) {
    console.log(error.message)
}