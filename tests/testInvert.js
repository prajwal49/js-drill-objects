import { invert } from "../invert.js";

const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };

try {
  const result = invert(testObject);
  console.log(result);
} catch (error) {
  console.log(error.message);
}
