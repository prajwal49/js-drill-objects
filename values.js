export function values(obj){
    const result = []
    for(let value in obj){
        if(obj.hasOwnProperty(value)){
            result.push(obj[value]);
        }
        
    }
    return result;
}